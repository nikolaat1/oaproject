<?php
/**
 * @file
 * is_project_paragraph.features.inc
 */

/**
 * Implements hook_views_api().
 */
function is_project_paragraph_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
