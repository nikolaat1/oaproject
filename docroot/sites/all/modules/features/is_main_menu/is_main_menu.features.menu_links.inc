<?php
/**
 * @file
 * is_main_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function is_main_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_user-page:user.
  $menu_links['main-menu_user-page:user'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'User Page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_user-page:user',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 6,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('User Page');

  return $menu_links;
}
