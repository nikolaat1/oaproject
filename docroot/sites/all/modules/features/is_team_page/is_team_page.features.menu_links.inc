<?php
/**
 * @file
 * is_team_page.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function is_team_page_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_team-page:team-page.
  $menu_links['main-menu_team-page:team-page'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'team-page',
    'router_path' => 'team-page',
    'link_title' => 'Team Page',
    'options' => array(
      'identifier' => 'main-menu_team-page:team-page',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Team Page');

  return $menu_links;
}
