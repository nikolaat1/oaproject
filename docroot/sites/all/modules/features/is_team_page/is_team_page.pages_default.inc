<?php
/**
 * @file
 * is_team_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function is_team_page_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'team_page';
  $page->task = 'page';
  $page->admin_title = 'Team Page';
  $page->admin_description = 'page that show all users (information)';
  $page->path = 'team-page';
  $page->access = array(
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Team Page',
    'name' => 'main-menu',
    'weight' => '3',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_team_page__panel';
  $handler->task = 'page';
  $handler->subtask = 'team_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel for team projects',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Team page ';
  $display->uuid = 'de08b639-cee3-4d6e-a530-7385b626f163';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_team_page__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-702edca5-efea-41a5-8e7e-4824195769da';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'team_page_users-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '702edca5-efea-41a5-8e7e-4824195769da';
  $display->content['new-702edca5-efea-41a5-8e7e-4824195769da'] = $pane;
  $display->panels['middle'][0] = 'new-702edca5-efea-41a5-8e7e-4824195769da';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['team_page'] = $page;

  return $pages;

}
