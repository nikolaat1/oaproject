<?php
/**
 * @file
 * is_contact_page.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function is_contact_page_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_contact-user:node/9.
  $menu_links['main-menu_contact-user:node/9'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/9',
    'router_path' => 'node/%',
    'link_title' => 'Contact User',
    'options' => array(
      'identifier' => 'main-menu_contact-user:node/9',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact User');

  return $menu_links;
}
