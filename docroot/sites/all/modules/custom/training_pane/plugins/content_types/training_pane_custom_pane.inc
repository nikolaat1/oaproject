<?php
/**
 * @file
 * Description.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Training Pane'),
  'description' => t('Training pane description'),
  'category' => t('My Module'),
  'edit form' => 'training_pane_custom_pane_edit_form',
  'render callback' => 'training_pane_custom_pane_render',
  'admin info' => 'mymod_pane_custom_pane_admin_info',
  'defaults' => array(
    'text' => '',
  ),
  'all contexts' => TRUE,
);
/**
 * An edit form for the pane's settings.
 */
function training_pane_custom_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

//  $form['text'] = array(
//    '#type' => 'textfield',
//    '#title' => t('Panel Text'),
//    '#description' => t('Text to display, it may use substitution strings'),
//    '#default_value' => $conf['text'],
//  );

  return $form;
}

/**
 * Submit function, note anything in the formstate[conf] automatically gets saved.
 * Notice, the magic that automatically does that for you.
 */
function training_pane_custom_pane_edit_form_submit(&$form, &$form_state) {
//  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
//    if (isset($form_state['values'][$key])) {
//      $form_state['conf'][$key] = $form_state['values'][$key];
//    }
//  }
}


/**
 * Run-time rendering of the body of the block (content type).
 * See ctools_plugin_examples for more advanced info.
 */
function training_pane_custom_pane_render($subtype, $conf, $args, $contexts) {
  global $user;
  $userId = $user->uid;

  drupal_add_library('system', 'ui.progressbar');

  $percent = training_pane_custom_get_user_available($userId);

  $block = new stdClass();
  drupal_add_js(drupal_get_path('module', 'progress_bar') . '/js/progress_bar.js');

  drupal_add_js(array(
    'training_pane' => array(
      'barValue' => $percent,
    ),
  ), 'setting');
  $block->content = '<div id="progressdiv"></div>';
  return $block;
}

/**
 * @param $view
 */
//function training_pane_views_pre_render(&$view) {
//
//  if ($view->name == 'team_page_users' && $view->current_display == 'panel_pane_1') {
//    drupal_add_library('system', 'ui.progressbar');
//    $result = &$view->result;
//    $userPercentage = [];
//    foreach ($result as $key => $value) {
//      $uid = $value->uid;
//      $percent = training_pane_custom_get_user_available($uid);
//      $userPercentage[$uid] = $percent;
//    }
//    drupal_add_js(drupal_get_path('module', 'progress_bar') . '/js/progress_bar_all_users.js');
//
//    drupal_add_js(array(
//      'training_pane' => array(
//        'percentage_array' => $userPercentage,
//      ),
//    ), 'setting');
//  }
//}

//  /**
//   * @param $userId
//   * @return float|int
//   *   Custom function for getting user available %.
//   */
//function training_pane_custom_get_user_available($userId) {
//  // Get current user capacity.
//  $query = db_select('field_data_field_capacity', 'capacity');
//  $query->fields('capacity', ['field_capacity_value']);
//  $query->condition('capacity.entity_id', $userId, '=');
//  $capacity = $query->execute()->fetchField();
//
//  $hoursQuery = db_select('field_data_field_project_paragraph', 'dataProjParag');
//  $hoursQuery->fields('dataProjParag', array('field_project_paragraph_value'))
//    ->condition('dataProjParag.entity_id', $userId, '=');
//  $projectParag = $hoursQuery->execute()->fetchAll(PDO::FETCH_COLUMN);
//  //$projectParag = $hoursQuery->execute()->fetchField();
//  //$projectParag imam masiv ot id na projectparagraph 0->2, 1->9
//
//  if (!empty($projectParag)) {
//    $hoursQuery = db_select('field_data_field_planing2', 'planingT');
//    $hoursQuery->fields('planingT', array('field_planing2_value'))
//      ->condition('planingT.entity_id', $projectParag, 'IN');
//    $monthParag = $hoursQuery->execute()->fetchAll(PDO::FETCH_COLUMN);
//  }
//
//  $monthHoursArr = 0;
//  if (!empty($monthParag)) {
//    $monthQuery = db_select('field_data_field_month_paragraph', 'months');
//    $monthQuery->join('field_data_field_booked_hours_pragraph', 'hours', 'months.entity_id = hours.entity_id');
//    $monthQuery->fields('hours', array('field_booked_hours_pragraph_value'))
//      ->condition('months.entity_id', $monthParag, 'IN');
//    $monthQuery->condition('months.field_month_paragraph_value', date('Y-m-01 00:00:00'), '=');
//
//    // @TODO: Add condition to get the hours only for the current month.
//    $monthHoursArr = $monthQuery->execute()->fetchAll(PDO::FETCH_COLUMN);
//  }
//  $hoursMonthSum = array_sum($monthHoursArr);
//  $percent = $hoursMonthSum / $capacity * 100;
//
//  return $percent;
//}