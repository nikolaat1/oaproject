(function ($) {
  Drupal.behaviors.progress_bar_all_users = {
    attach: function (context, settings) {
      let percentageArray = settings.training_pane.percentage_array;
      for(var key in percentageArray){
          $percent = percentageArray[key];
          $(".progressbarclass-"+key)
          .progressbar({
            value: 0
          });
          let progresbarValue = $('.progressbarclass-'+key+' .ui-progressbar-value');
          $(".progressbarclass-"+key).children('.ui-progressbar-value')
          .html($percent.toPrecision(3) + '%')
          .css({"display": "block", "text-align": "center", "background" : "green"});
          if ($percent < 100){
            progresbarValue
            .animate({width: $percent + '%', opacity: '1'}, 1500);
          } else{
            progresbarValue.css({"display": "block", "text-align": "center", "background" : "red"})
            .animate({width: 100 + '%', opacity: '1'}, 1500);
          }
        }
      }

    }
}(jQuery));

