(function ($) {
  Drupal.behaviors.progress_bar = {
    attach: function (context, settings) {
        let barVal = settings.training_pane.barValue;

        $("#progressdiv")
            .progressbar({
                value: 0
            });
        let progresbarValue = $('.ui-progressbar-value');
        $('#progressdiv').children('.ui-progressbar-value')
        .html(barVal.toPrecision(3) + '%')
        .css({"display": "block", "text-align": "center", "background" : "green"});
        if (barVal < 100){
            progresbarValue
               .animate({width: barVal + '%', opacity: '1'}, 1500);
        } else{
            progresbarValue.css({"display": "block", "text-align": "center", "background" : "red"})
                .animate({width: 100 + '%', opacity: '1'}, 1500);
        }

      }
    }
}(jQuery));

